CREATE USER quiz_engine_user_dev WITH PASSWORD 'mysecretpassword1';

CREATE DATABASE quiz_engine_backend_dev;
GRANT ALL PRIVILEGES ON DATABASE quiz_engine_backend_dev TO quiz_engine_user_dev;

CREATE DATABASE quiz_engine_backend_test;
GRANT ALL PRIVILEGES ON DATABASE quiz_engine_backend_test TO quiz_engine_user_dev;
